#ifndef BLOCKALLOC_HPP_
#define BLOCKALLOC_HPP_

#include <stddef.h>
#include <cstring>
#include <vector>
#include <utility>

template<class allocator_type>
class blockalloc
{
public:
	blockalloc();
	~blockalloc();

	// Allocate space for one "allocator_type"
	allocator_type* malloc();

	// De-allocate space for one "allocator_type"
	void free(allocator_type* pointer);

	// Allocate space and call the contructor
	allocator_type* new_object();

	// Call the destructor and de-allocate the space
	void delete_object(allocator_type* pointer);

//private:
	// How many objects shall be allocated at once
	const size_t n_reserve = 32;

	// Reserve space for "n_reserve" objects
	void reserve();

	class ringbuffer
	{
	public:
		ringbuffer();
		virtual ~ringbuffer();

		void push(allocator_type* e);
		allocator_type* pop();

		size_t size();

		void resize(size_t offset);

		class iterator
		{
		public:
			void operator++();
			allocator_type*& operator*();
			bool operator!=(iterator& it);

//		private:
			iterator(ringbuffer* ring, size_t i);

			size_t first, i;
			ringbuffer* ring;

			friend ringbuffer;
		};

		iterator begin();
		iterator end();

//	private:
		size_t _first, _size;
		std::vector<allocator_type*> _array;

		const size_t n_reserve = 32;
	};

	ringbuffer _chunks;
	ringbuffer pointers;
};

template<class allocator_type>
inline blockalloc<allocator_type>::blockalloc() :
		_chunks(), pointers()
{
	// Reserve some space
	reserve();
}

template<class allocator_type>
inline blockalloc<allocator_type>::~blockalloc()
{
	// Delete every chunk
	for (allocator_type* p : _chunks)
	{
		delete[] p;
	}
}

template<class allocator_type>
inline void blockalloc<allocator_type>::reserve()
{
	// Resize Chunk buffer
	_chunks.resize(1);

	// Resize the Pointer buffer
	pointers.resize(n_reserve);

	// Allocate a block
	allocator_type* block = new allocator_type[n_reserve];

	// Register it as a chunk
	_chunks.push(block);

	// Insert its elements into the pointer buffer
	for (size_t i = 0; i < n_reserve; ++i)
	{
		pointers.push(block + i);
	}
}

template<class allocator_type>
inline allocator_type* blockalloc<allocator_type>::malloc()
{
	// If all pointers are used, allocate more
	if (pointers.size() == 0)
	{
		reserve();
	}

	// Use one pointer
	allocator_type* pointer = pointers.pop();
	return pointer;
}

template<class allocator_type>
inline allocator_type* blockalloc<allocator_type>::new_object()
{
	// Allocate space
	allocator_type* pointer = malloc();

	// Construct an object
	allocator_type tpl;

	// Copy its content into the pointer
	memcpy(pointer, &tpl, sizeof(allocator_type));

	// Return the pointer
	return pointer;
}

template<class allocator_type>
inline void blockalloc<allocator_type>::delete_object(allocator_type* pointer)
{
	// Call the destructor
	pointer->~allocator_type();

	// Return it to the pool
	free(pointer);
}

template<class allocator_type>
inline void blockalloc<allocator_type>::free(allocator_type* pointer)
{
	// Return it to the pool
	pointers.push(pointer);
}

template<class allocator_type>
inline blockalloc<allocator_type>::ringbuffer::ringbuffer() :
		_array()
{
	_first = 0;
	_size = 0;
}

template<class allocator_type>
inline blockalloc<allocator_type>::ringbuffer::~ringbuffer()
{
}

template<class allocator_type>
inline void blockalloc<allocator_type>::ringbuffer::push(allocator_type* e)
{
	if (_size == _array.size())
		resize(n_reserve);
	_array[(_first + _size) % _array.size()] = e;
	++_size;
}

template<class allocator_type>
inline allocator_type* blockalloc<allocator_type>::ringbuffer::pop()
{
	--_size;
	allocator_type* element = _array[_first];
	_first = (_first + 1) % _array.size();
	return element;
}

template<class allocator_type>
inline size_t blockalloc<allocator_type>::ringbuffer::size()
{
	return _size;
}

template<class allocator_type>
inline void blockalloc<allocator_type>::ringbuffer::resize(size_t offset)
{
	std::vector<allocator_type*> to_swap(_size + offset);
	for (size_t i = 0; i < _size; ++i)
	{
		to_swap[i] = _array[(_first + i) % _array.size()];
	}
	std::swap(_array, to_swap);
	_first = 0;
}

template<class allocator_type>
inline blockalloc<allocator_type>::ringbuffer::iterator::iterator(
		ringbuffer* ring, size_t i)
{
	this->ring = ring;
	this->first = ring->_first;
	this->i = i;
}

template<class allocator_type>
inline void blockalloc<allocator_type>::ringbuffer::iterator::operator ++()
{
	++i;
}

template<class allocator_type>
inline allocator_type*& blockalloc<allocator_type>::ringbuffer::iterator::operator *()
{
	return ring->_array[(first + i) % ring->_array.size()];
}

template<class allocator_type>
inline bool blockalloc<allocator_type>::ringbuffer::iterator::operator !=(
		iterator& it)
{
	return !(it.ring->_array == ring->_array && it.i == i);
}

template<class allocator_type>
inline typename blockalloc<allocator_type>::ringbuffer::iterator blockalloc<
		allocator_type>::ringbuffer::begin()
{
	return iterator(this, 0);
}

template<class allocator_type>
inline typename blockalloc<allocator_type>::ringbuffer::iterator blockalloc<
		allocator_type>::ringbuffer::end()
{
	return iterator(this, _size);
}

#endif
