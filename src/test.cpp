#include <stddef.h>
#include <iostream>

#include "blockalloc.hpp"

const size_t test_count = 2048 * 16;

class Test
{
public:
	Test();
	virtual ~Test();
	size_t a;
};

inline Test::Test()
{
	a = 100;
}

inline Test::~Test()
{
	a = 200;
}

bool test_ringbuffer()
{
	blockalloc<int>::ringbuffer buff;

	int* b = (int*) 0;
	buff.push(b);
	buff.push(b);
	buff.pop();
	buff.pop();

	for (size_t i = 0; i < 256; i++)
	{
		int* a = (int*) (i + 1);
		buff.push(a);
	}

	return true;
}

bool test_simple()
{
	clock_t t_start, t_end, t_duration;

	t_start = clock();

	for (size_t i = 0; i < test_count; i++)
	{
		size_t* a = (size_t*) malloc(sizeof(size_t));
		*a = i;
		if (*a != i)
			return false;
	}

	t_end = clock();
	t_duration = t_end - t_start;

	std::cout << "Simple Allocator: " << (t_duration) << std::endl;

	blockalloc<size_t> allocator;

	t_start = clock();

	for (size_t i = 0; i < test_count; i++)
	{
		size_t* a = allocator.malloc();
		*a = i;
		if (*a != i)
			return false;
	}

	t_end = clock();
	t_duration = t_end - t_start;

	std::cout << "Pool Allocator: " << (t_duration) << std::endl;

	return true;
}

bool test_objects()
{
	blockalloc<Test> allocator;

	clock_t t_start, t_end, t_duration;

	t_start = clock();
	for (size_t i = 0; i < test_count; i++)
	{
		Test* a = new Test();
		if (a->a != 100)
			return false;
	}
	t_end = clock();
	t_duration = t_end - t_start;

	std::cout << "Simple Allocator: " << (t_duration) << std::endl;

	t_start = clock();
	for (size_t i = 0; i < test_count; i++)
	{
		Test* a = allocator.new_object();
		if (a->a != 100)
			return false;
	}
	t_end = clock();
	t_duration = t_end - t_start;

	std::cout << "Pool Allocator: " << (t_duration) << std::endl;

	return true;
}

int main()
{
	std::cout << "Test 1: "
			<< (test_ringbuffer() ? "Bestanden" : "Fehlgeschlagean")
			<< std::endl;

	std::cout << "Test 2: " << (test_simple() ? "Bestanden" : "Fehlgeschlagen")
			<< std::endl;

	std::cout << "Test 3: " << (test_objects() ? "Bestanden" : "Fehlgeschlagen")
			<< std::endl;

	return 0;
}
